# QAuto-Admin Guide

The guide is build on the mkdocs documentation generator (http://www.mkdocs.org/).

# Installation

    pip install -r requirements.txt

See mkdocs documentation for installation (http://www.mkdocs.org/#installation)

The theme is based on (https://github.com/squidfunk/mkdocs-material). Please see their documentation for installation (https://squidfunk.github.io/mkdocs-material/).

# Build the guide

    cd guide
    mkdocs serve

Open a browser at `http://localhost:8000` and enjoy the documentation.



