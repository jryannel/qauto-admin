import click
import sh
from path import Path
import platform
from .shell import makedirs, rmtree, run
import logging

_log = logging.getLogger(__name__)

is_linux = platform.system() == 'Linux'
is_macos = platform.system() == 'Darwin'


def echo_dot(msg):
    msg = msg.strip()
    logging.debug(msg)
    click.secho('.', fg='blue', nl=False)


def echo_header(title):
    click.echo()
    click.secho(40 * '#', fg='green')
    click.secho('\t{0}'.format(title))
    click.secho(40 * '#', fg='green')


def echo(msg):
    msg = msg.strip()
    logging.info(msg)
    click.secho(msg, fg='blue')


def error(msg):
    if msg.startswith('Project MESSAGE:'):
        return
    msg = msg.strip()
    logging.warn(msg)
    click.secho(msg, fg='red')


def get_builder(repo, config):
    if repo.build == 'qmake':
        return QMakeBuilder(repo, config)
    if repo.build == 'cmake':
        return CMakeBuilder(repo, config)


make = sh.Command('make').bake(_out=echo, _err=error)


class Repo(object):
    def __init__(self, name, data):
        self.name = name
        self.url = data.get('url', None)
        self.build = data.get('build', None)
        self.branch = data.get('branch', 'master')
        self.os = data.get('os')
        self.codereview = data.get('codereview', '')
        self.is_qt_module = data.get('qt_module', False)


class BuildConfig(object):
    def __init__(self, source_root, build_root, install_root, qmake, jobs=1):
        self.source_root = Path(source_root).abspath()
        self.build_root = Path(build_root).abspath()
        self.install_root = Path(install_root).abspath()
        self.qmake = Path(qmake).abspath() if qmake else Path()
        self.jobs = jobs

        self.qt_root = self.qmake.parent.parent
        self.is_valid = self._validate()

    def _validate(self):
        if not self.source_root.exists() or not self.source_root.isdir():
            click.secho('invalid source {}'.format(self.source_root), fg='red')
            return False
        if not self.build_root.exists() or not self.build_root.isdir():
            click.secho('invalid build {}'.format(self.build_root), fg='red')
            return False
        if not self.install_root.exists() or not self.install_root.isdir():
            click.secho('invalid install {}'.format(self.install_root), fg='red')
            return False
        if not self.qmake.exists() or not self.qmake.isfile():
            click.secho('invalid qmake {}. Please set qmake using `qauto-admin config qmake <path>`'.format(self.qmake), fg='red')
            return False
        if not self.jobs > 0:
            click.secho('make jobs must be greater 0', fg='red')
            return False
        return True

    def dump(self):
        click.echo('source_root: {}'.format(self.source_root))
        click.echo('build_root: {}'.format(self.build_root))
        click.echo('install_root: {}'.format(self.install_root))
        click.echo('qmake: {}'.format(self.qmake))
        click.echo('jobs: {}'.format(self.jobs))


class Builder(object):
    def __init__(self, repo, config):
        self.is_valid = True
        self.repo = repo
        self.source_root = config.source_root
        self.install_root = config.install_root
        self.build_root = config.build_root
        self.qt_root = config.qt_root
        self.qmake_path = config.qmake
        self.jobs = config.jobs
        self.qmake = self.qmake_path
        self.source_path = self.source_root / self.repo.name
        self.build_path = self.build_root / self.repo.name
        self.install_path = config.install_root / self.repo.name

        self.validate()
        if not self.is_valid:
            click.echo('no valid build configuration. exit')

    def validate_path(self, path, name):
        if not path.exists():
            click.echo('{} not valid: {}'.format(name, path))
            self.is_valid = False
            return False
        return True

    def ensure_path(self, path):
        if not path.exists():
            makedirs(path)

    def check_os(self):
        if is_linux and 'linux' not in self.repo.os:
            click.echo('linux is not supported')
            self.is_valid = False
        if is_macos and 'macos' not in self.repo.os:
            click.echo('macos is not supported')
            self.is_valid = False

    def validate(self):
        self.validate_path(self.source_path, 'source path')
        self.ensure_path(self.build_path)
        self.check_os()

    def install(self):
        """installs the build"""
        click.echo('install {}'.format(self.repo.name))
        if not self.is_valid:
            return
        run('make install', self.build_path)

    def rebuild(self):
        """rebuilds the source"""
        click.echo('rebuild {}'.format(self.repo.name))
        if not self.is_valid:
            return
        self.clean()
        self.build()

    def configure(self, pause=True):
        """configure the source"""
        if not self.is_valid:
            return
        click.echo('configure {}'.format(self.repo.name))

    def build(self):
        """build the source code"""
        if not self.is_valid:
            return
        click.echo('build {}'.format(self.repo.name))
        self.ensure_path(self.build_path)
        run('make --jobs={}'.format(self.jobs), self.build_path)

    def clean(self):
        """clean the build folder"""
        if not self.is_valid:
            return
        click.echo('clean {}'.format(self.repo.name))
        rmtree(self.build_path)
        rmtree(self.install_path)


class QMakeBuilder(Builder):
    def configure(self, pause=True):
        if self.repo.is_qt_module:
            run('{} {}'.format(self.qmake, self.source_path), self.build_path)
        else:
            run('{} {} INSTALL_PREFIX={}'.format(self.qmake, self.source_path, self.install_root), self.build_path)
        if pause:
            click.pause()


class CMakeBuilder(Builder):
    def validate(self):
        super(CMakeBuilder, self).validate()

    def configure(self, pause=True):
        if self.repo.is_qt_module:
            run('cmake {} -DCMAKE_PREFIX_PATH={}/lib/cmake'.format(self.source_path, self.qt_root), self.build_path)
        else:
            run('cmake {} -DCMAKE_PREFIX_PATH={}/lib/cmake -DCMAKE_INSTALL_PREFIX={}'.format(self.source_path, self.qt_root, self.install_root), self.build_path)

        if pause:
            click.pause()
