import logging
import sys
from path import Path
from jinja2 import Environment, Undefined, StrictUndefined
from jinja2 import FileSystemLoader, ChoiceLoader
from jinja2 import TemplateSyntaxError, TemplateNotFound, TemplateError
import click
import hashlib
from .shell import makedirs
from .options import dry_run, relpathto, echo

logger = logging.getLogger(__name__)

filters = {}


def template_error_handler(traceback):
    exc_type, exc_obj, exc_tb = traceback.exc_info
    error = exc_obj
    if isinstance(exc_type, TemplateError):
        error = exc_obj.message
    message = '{0}:{1}: error: {2}'.format(exc_tb.tb_frame.f_code.co_filename, exc_tb.tb_lineno, error)
    click.secho(message, fg='red', err=True)


class TestableUndefined(StrictUndefined):
    """Return an error for all undefined values, but allow testing them in if statements"""

    def __bool__(self):
        return False


class Generator(object):
    strict = True

    def __init__(self, path, context={}):
        loader = ChoiceLoader([
            FileSystemLoader(path),
        ])
        self.env = Environment(
            loader=loader,
            trim_blocks=True,
            lstrip_blocks=True
        )
        self.env.exception_handler = template_error_handler
        self.env.filters.update(filters)
        self._destination = Path()
        self._source = ''
        self.context = context
        self.dry_run = dry_run()

    @property
    def destination(self):
        """destination prefix for generator write"""
        return self._destination

    @destination.setter
    def destination(self, dst):
        if dst:
            self._destination = Path(self.apply(dst, self.context))

    @property
    def source(self):
        """source prefix for template lookup"""
        return self._source

    @source.setter
    def source(self, source):
        if source:
            self._source = source

    @property
    def filters(self):
        return self.env.filters

    @filters.setter
    def filters(self, filters):
        self.env.filters.update(filters)

    def apply(self, template, context):
        """Return the rendered text of a template instance"""
        return self.env.from_string(template).render(context)

    def render(self, name, context):
        """Returns the rendered text from a single template file from the
        template loader using the given context data"""
        if Generator.strict:
            self.env.undefined = TestableUndefined
        else:
            self.env.undefined = Undefined
        template = self.get_template(name)
        return template.render(context)

    def get_template(self, name):
        """Retrieves a single template file from the template loader"""
        source = name
        # remove leading '/'
        if name.startswith('/'):
            source = name[1:]
        # prepend source
        elif self.source:
            source = '/'.join((self.source, name))
        return self.env.get_template(source)

    def write(self, file_path, template=None, context={}, preserve=False, force=False):
        """Using a template file name it renders a template
           into a file given a context
        """
        template = template or file_path
        if not context:
            context = self.context
        error = False
        try:
            self._write(file_path, template, context, preserve, force)
        except TemplateSyntaxError as exc:
            message = '{0}:{1}: error: {2}'.format(exc.filename, exc.lineno, exc.message)
            click.secho(message, fg='red', err=True)
            error = True
        except TemplateNotFound as exc:
            message = '{0}: error: Template not found'.format(exc.name)
            click.secho(message, fg='red', err=True)
            error = True
        except TemplateError as exc:
            # Just return with an error, the generic template_error_handler takes care of printing it
            error = True

        if error and Generator.strict:
            sys.exit(1)

    def _write(self, file_path, template, context, preserve=False, force=False):
        path = self.destination / Path(self.apply(file_path, context))
        makedirs(path.parent, silent=True)
        logger.info('write {0}'.format(path))
        data = self.render(template, context)
        if self._has_different_content(data, path) or force:
            if path.exists() and preserve:
                echo('preserve {0}'.format(relpathto(path)))
            else:
                echo('write {0}'.format(relpathto(path)))
                if not self.dry_run:
                    path.open('w', encoding='utf-8').write(data)

    def _has_different_content(self, data, path):
        if not path.exists():
            return True
        dataHash = hashlib.new('md5', data.encode('utf-8')).digest()
        pathHash = path.read_hash('md5')
        return dataHash != pathHash

    def register_filter(self, name, callback):
        """Register your custom template filter"""
        self.env.filters[name] = callback
