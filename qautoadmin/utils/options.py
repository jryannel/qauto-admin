from path import Path
import yaml
import click
import os
import sys
from string import Template
import logging

_log = logging.getLogger(__name__)


resolved_workspace_path = None


def resolve_workspace(cwd):
    # try to hit the cache
    global resolved_workspace_path
    if resolved_workspace_path:
        return resolved_workspace_path
    # try to resolve workspace path
    path = Path(cwd).expand()
    if path == HOME_DIR:
        return None
    while True:
        if Path(path / CONFIG_NAME).exists():
            click.secho('using workspace `~/{}`'.format(HOME_DIR.relpathto(path)), fg='yellow')
            resolved_workspace_path = path
            return path
        if path.parent == path:
            break
        if path.parent == HOME_DIR:
            break
        path = path.parent
    return None


CONFIG_NAME = 'qauto.yml'
HOME_DIR = Path('~').expanduser()
CWD = Path.getcwd()
VERBOSE = False
DRY_RUN = False
WS_PATH = resolve_workspace(CWD)
ENV = {}


def info(msg):
    """standard info call"""
    click.secho(msg.strip(), fg='green')


def echo(msg):
    """standard echo call"""
    click.secho(msg.strip(), fg='blue')


def warning(msg):
    click.secho('WARN: {}'.format(msg.strip()), fg='magenta')


def error(msg, error_code=-1):
    """standard error call"""
    click.secho("ERROR: {}".format(msg.strip()), fg='red')
    sys.exit(error_code)


def run_echo(msg, path=CWD):
    """standard echo call with optional path component"""
    dry = "DRY :" if dry_run() else ""
    click.secho('[{0}{1:16}]> '.format(dry, CWD.relpathto(path)), fg='green', nl=False)
    click.secho(msg, fg='blue')


def get_env():
    return ENV


def get_ws_path():
    return WS_PATH


def dry_run():
    return DRY_RUN


def verbose():
    return VERBOSE


def relpathto(path):
    if WS_PATH:
        return WS_PATH.relpathto(path)
    return CWD.relpathto(path)


# the central configuration, passed onto the commands
class Options(object):
    def __init__(self):
        self.data = {}
        self.targets = {}
        self.repos = {}
        self.scripts = {}
        self.config = {}
        self.env = os.environ.copy()
        # config
        self.env_name = '.env'
        self.workspace_path = WS_PATH
        if not self.workspace_path:
            self.exit('Not a qauto project (or any of the parent directories). Run init first.')
        self.config_path = self.workspace_path / CONFIG_NAME
        self.env_path = self.workspace_path / self.env_name
        self._read()
        self._read_env()

        self.source_path.makedirs_p()
        self.build_path.makedirs_p()
        self.install_path.makedirs_p()
        self.dev_source_path.makedirs_p()
        self.dev_build_path.makedirs_p()
        self.dev_install_path.makedirs_p()
        global ENV
        ENV = self.env

    def exit(self, reason):
        """prints the reason and exits the script"""
        click.secho(reason, fg='red')
        sys.exit(-1)

    def log(self, msg, *args):
        """logs a message to stderr."""
        if args:
            msg %= args
        click.echo(msg, file=sys.stderr)

    def vlog(self, msg, *args):
        """logs a message to stderr only if verbose is enabled."""
        if VERBOSE:
            self.log(msg, *args)

    def _write(self):
        self.data['targets'] = self.targets
        self.data['repos'] = self.repos
        self.data['scripts'] = self.scripts
        self.data['config'] = self.config
        text = yaml.safe_dump(self.data, default_flow_style=False)
        self.config_path.write_text(text)

    def _read(self):
        if not self.config_path.exists():
            return
        text = self.config_path.text()
        self.data.update(yaml.load(text))
        self.repos = self.data.get('repos', {}) or {}
        self.targets = self.data.get('targets', {}) or {}
        self.scripts = self.data.get('scripts', {}) or {}
        self.config = self.data.get('config', {}) or {}
        d = self.data.get('env', {}) or {}
        # convert k,v to str
        # TODO: missing veriable substitution
        # Best to extract the whole env handling into own class
        d = {str(k): str(d[k]) for k in d}
        self.env.update(d)

    def set_value(self, name, value):
        value = str(value)
        echo('config {0}={1}'.format(name, value))
        self.config.update({name: value})
        self._write()

    def value(self, name, default=''):
        return self.config.get(name, default)

    def unset_value(self, name):
        echo('unset value {}'.format(name))
        del self.config[name]
        self._write()

    def repo_add(self, name, url, branch, build):
        self.repos.update({
            name: {'url': url, 'branch': branch, "build": build}
        })
        self._write()

    def repo_remove(self, name):
        if name in self.repos:
            del self.repos[name]
            self._write()

    def repo(self, name):
        if name not in self.repos:
            echo('unknown repo: {}'.format(name))
            return {}
        return self.repos.get(name, {})

    @property
    def source_path(self):
        return self.workspace_path / self.value('source', 'source')

    @property
    def dev_source_path(self):
        return self.workspace_path / self.value('dev', 'dev') / self.value('source', 'source')

    @property
    def build_path(self):
        return self.workspace_path / self.value('build', 'build')

    @property
    def dev_build_path(self):
        return self.workspace_path / self.value('dev', 'dev') / self.value('build', 'build')

    @property
    def install_path(self):
        return self.workspace_path / self.value('install', 'install')

    @property
    def dev_install_path(self):
        return self.workspace_path / self.value('dev', 'dev') / self.value('install', 'install')

    @property
    def qmake_path(self):
        return self.value('qmake', "")

    @property
    def make_jobs(self):
        return self.value('jobs', 2)

    def target_set(self, name, repos):
        self.targets[name] = repos
        self._write()
        echo('target `{}` set to {}'.format(name, ','.join(repos)))

    def target_remove(self, name):
        if name in self.targets:
            echo('removed target {}'.format(name))
            del self.targets[name]
            self._write()
        else:
            echo('target `{}` does not exist'.format(name))

    def target(self, name):
        return self.targets.get(name, [])

    def get_repos(self, target):
        """return the repo names based on the given target
        or the repo name if its a valid name"""
        repos = []
        if target == 'all':
            repos = self.repos.keys()
        elif target in self.targets:
            repos = self.targets[target]
        elif target in self.repos:
            repos = [target]
        return repos

    def _read_env(self):
        if self.env_path.exists():
            text = self.env_path.text()
            # substitude existing ${<var>}s
            text = Template(text).safe_substitute(self.env)
            d = yaml.safe_load(text)
            # convert k,v to str
            d = {str(k): str(d[k]) for k in d}
            self.env.update(d)

    def script(self, name):
        return self.scripts.get(name)

    def set_script(self, name, script):
        self.scripts[name] = script
        self._write()

    def update_scripts(self, scripts):
        scripts = scripts or {}
        self.scripts.update(scripts)
        self._write()

    def validate(self):
        if not self.config_path.exists():
            click.secho("fatal: Not a qauto workspace")
            sys.exit(-1)


# create a decorator to pass on the options object
pass_options = click.make_pass_decorator(Options, ensure=True)
