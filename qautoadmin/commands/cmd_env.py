from fnmatch import fnmatch
from qautoadmin.utils.options import pass_options, echo
import click
import logging

_log = logging.getLogger(__name__)


def echo_env(env):
    click.secho('{:_^16}_|_{:_^32}'.format('name', 'value'), fg="green")
    for name, value in env.items():
        click.secho('{:16} | {:<32}'.format(name, value), fg="green")


@click.command('env', short_help='display env variables')
@click.option('--edit', is_flag=True, help="opens the .env file in the editor")
@click.argument('name', required=False)
@pass_options
def app(opts, edit, name):
    """ displays the used environment variables
    """
    _log.info('env')
    if edit:
        click.edit(filename=opts.env_path)
    elif name:
        for key in opts.env:
            if fnmatch(key.lower(), name.lower()):
                echo("{}={}".format(key, opts.env[key]))
    else:
        echo_env(opts.env)
