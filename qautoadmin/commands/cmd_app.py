import click
from qautoadmin.utils.options import pass_options
from qautoadmin.utils.gen import Generator
from path import Path
import logging

_log = logging.getLogger(__name__)

here = Path(__file__).abspath().parent


@click.command('app', short_help='EXPERIMENTAL: creates a new application')
@click.argument('name', required=True, type=click.Path(resolve_path=True))
@pass_options
def app(opts, name):
    """EXPERIMENTAL: creates an application scaffold inside a project"""
    click.secho('EXPERIMENTAL FEATURE. USE WITH CARE.', fg='red')
    _log.info('create app in apps/%s' % click.format_filename(name))
    g = Generator(path=here / 'templates')
    g.destination = name
    g.write('Main.qml', 'app/Main.qml')
