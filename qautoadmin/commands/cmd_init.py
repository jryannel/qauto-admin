import click
from qautoadmin.utils.options import resolve_workspace
from qautoadmin.utils.gen import Generator
from qautoadmin.utils.shell import download
from path import Path
import logging


_log = logging.getLogger(__name__)

here = Path(__file__).abspath().parent


def generate_config(dst):
        g = Generator(path=here / 'templates', context={'dst': dst})
        g.destination = dst
        g.source = 'config'
        g.write('qauto.yml')


@click.command('init', help='creates an empty qauto workspace')
@click.option('--source', required=False, help="uses the config file from given location")
@click.option('--force/--no-force', required=False, help="overwrites an existing configuration if exists")
def app(source, force):
    """Initialized the workspace by writing the `qauto.yml` setup document"""
    _log.info('init')
    workspace_path = resolve_workspace(Path.getcwd())
    if workspace_path and not force:
        click.echo('workspace already exists. Delete first')
        return
    workspace_path = workspace_path or Path.getcwd()
    with workspace_path:
        if source:
            if source.endswith('qauto.yml'):
                Path('qauto.yml').remove_p()
                download(source, 'qauto.yml')
            else:
                click.secho('source needs to point to a `qauto.yml` document')
        else:
            generate_config(workspace_path)

