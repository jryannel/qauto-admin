import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import "stores"
import "panels"
import "helpers/apps.js" as Apps

Page {
    id: root

    property RootStore store: RootStore {
    }

    header: StatusBar {
        message: qsTr("App: %1").arg(store.currentApp)
    }

    Connections {
        target: store
        onShowApp: stack.showApp(app)
    }

    StackView {
        id: stack
        anchors.fill: parent
        function showApp(app) {
            stack.replace(app, {store: store})
        }
        Component.onCompleted: {
            showApp(Apps.welcome)
        }
    }


    footer: ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                text: qsTr("Welcome")
                onClicked: store.showView(Views.welcome)
            }
        }
    }
}
