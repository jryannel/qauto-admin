import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import "sysui"

ApplicationWindow {
    id: root
    width: 800
    height: 600
    visible: true


    MainWindow {
        id: window
        anchors.fill: parent
    }
}
