import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import "../helpers"

Pane {
    id: root
    property string message

    signal back()

    RowLayout {
        anchors.fill: parent
        Label {
            text: root.message
        }
        Label {
            id: echo
        }
        Item {
            Layout.fillWidth: true
        }
        ToolButton {
            text: "Back"
            onClicked: root.back();
        }
    }

    Connections {
        target: Notifier
        onNotify: {
            if(channel === 'echo') {
                echo.text = payload
            }
        }
    }
}
