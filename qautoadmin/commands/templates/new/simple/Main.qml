import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import "stores" as Stores
import "views" as Views
import "panels" as Panels

ApplicationWindow {
    id: root
    width: 800
    height: 600
    visible: true

    property alias store: store

    Stores.RootStore {
        id: store
        stackStore.views: views
        stackStore.initialView: 'welcome'
    }

    header: Panels.StatusBar {
        message: store.message
        onBack: root.store.popView()
    }


    Views.ViewRegistry {
        id: views
        store: root.store
    }

    Views.StackView {
        anchors.fill: parent
        store: root.store.stackStore
    }
}
