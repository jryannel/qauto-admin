import QtQuick 2.0
import sys.ui 1.0

import "../helpers/viewstack.js" as ViewStack

Store {
    id: root

    objectName: 'StackStore'

    property var views;

    signal requestView(string view, Component component, var args)


    property string initialView
    property string currentView: ViewStack.last()

    function showView(name, args) {
        args = args || {};
        if (!name) { return; }
        ViewStack.push(name);
        var component = views.get(name);
        requestView(name, component, args);
        return name;
    }

    function popView(args) {
        args = args || {};
        var name = ViewStack.pop();
        var component = views.get(name);
        requestView(name, component, args);
        return name;
    }

    Component.onCompleted: {
        showView(initialView);
    }
}
