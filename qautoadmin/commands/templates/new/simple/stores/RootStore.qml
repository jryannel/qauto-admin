import QtQuick 2.10
import sys.ui 1.0

Store {
    id: root
    objectName: "RootStore"

    property string message

    property CounterStore counterStore: CounterStore {
        onCountChanged: {
            root.message = "count: " +  count
        }
    }

    property StackStore stackStore: StackStore {
    }

    function showView(name, args) {
        return stackStore.showView(name, args);
    }

    function popView(args) {
        return stackStore.popView(args);
    }

    Component.onCompleted: {
        message = "ready"
    }
}
