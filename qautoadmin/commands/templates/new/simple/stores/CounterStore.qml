import QtQuick 2.0
import sys.ui 1.0

Store {
    id: root
    objectName: "CounterStore"

    property int count: 0

    property var increment: function() {
        count++;
    }

    property var decrement: function() {
        if (count>0) {
            count--;
        }
    }

}
