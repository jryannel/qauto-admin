
var _stack = []

function last() {
    var value = _stack.slice(-1)[0];
    return value?value:'';
}

function first() {
    var value = _stack.slice(0)[0];
    return value?value:'';
}

function push(view) {
    return _stack.push(view);
}

function pop() {
    return _stack.pop();
}
