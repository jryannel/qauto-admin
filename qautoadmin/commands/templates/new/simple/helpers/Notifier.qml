pragma Singleton
import QtQuick 2.10

QtObject {
    id: root

    function send(channel, payload) {
        notify(channel, payload);
    }

    signal notify(string channel, var payload);
}