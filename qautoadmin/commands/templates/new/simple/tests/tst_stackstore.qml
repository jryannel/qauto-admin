import QtQuick 2.0
import QtTest 1.2

import "../stores" as Stores

TestCase {
    name: "StackStore"

    Component {
        id: storeComponent
        Stores.StackStore {
            views: QtObject {
                function get(name) {
                    return 'welcome';
                }
            }
        }
    }

    SignalSpy {
        id: spy
    }

    function createStore() {
        return createTemporaryObject(storeComponent);
    }

    function setUp() {
        spy.clear();
    }

    function test_showView() {
        var name = 'welcome';
        var store = createStore();
        compare(spy.count, 0)
        spy.target = store
        spy.signalName = "requestView"
        store.showView(name);
        compare(spy.count, 1)
    }

    function test_pop() {
    }

}