import QtQuick 2.0
import QtTest 1.2

import "../helpers/viewstack.js" as ViewStack
TestCase {
    name: "Helpers"

    function test_push() {
        var name = 'welcome';
        ViewStack.push(name);
        compare(ViewStack.first(), ViewStack.last());
        compare(ViewStack.first(), name);
        compare(ViewStack.last(), name);
    }

    function test_pop() {
        var name = 'welcome';
        ViewStack.push(name);
        compare(ViewStack.first(), name);
        ViewStack.pop();
        compare(ViewStack.first(), '');

    }

}