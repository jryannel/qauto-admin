import QtQuick 2.0
import "../stores"

Item {
    property RootStore store

    Component {
        id: welcomeView
        WelcomeView {
            store: root.store
        }
    }
    Component {
        id: counterView
        CounterView {
            store: root.store.counterStore
        }
    }

    property var _registry: ({
        welcome: welcomeView,
        counter: counterView,
    })

    function get(name) {
        return _registry[name];
    }
}
