import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import sys.ui 1.0

import "../stores"
import "../helpers"

View {
    id: root
    objectName: "counter"

    property CounterStore store

    Label {
        anchors.centerIn: parent
        text: store.count
    }


    footer: ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                Layout.fillWidth: true
                text: qsTr("Increment")
                onClicked: root.store.increment()
            }
            ToolButton {
                Layout.fillWidth: true
                text: qsTr("Decrement")
                onClicked: root.store.decrement()
            }
            ToolButton {
                Layout.fillWidth: true
            }
            ToolButton {
                Layout.fillWidth: true
            }
            ToolButton {
                Layout.fillWidth: true
            }
            ToolButton {
                Layout.fillWidth: true
            }
        }
    }
}
