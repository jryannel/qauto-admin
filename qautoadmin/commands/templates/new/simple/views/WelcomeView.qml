import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import "../stores"
import "../helpers"

Page {
    id: root

    property RootStore store
    objectName: "welcome"


    footer: ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                text: qsTr("Counter")
                Layout.fillWidth: true
                onClicked: root.store.showView('counter')
            }
            ToolButton {
                Layout.fillWidth: true
                text: "Send Echo"
                onClicked: Notifier.send("echo", "hello");
            }
            ToolButton {
                Layout.fillWidth: true
            }
            ToolButton {
                Layout.fillWidth: true
            }
            ToolButton {
                Layout.fillWidth: true
            }
            ToolButton {
                Layout.fillWidth: true
            }
        }
    }
}
