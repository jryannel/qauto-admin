import QtQuick 2.10
import QtQuick.Controls 2.3 as QQC2
import QtQuick.Layouts 1.3

import sys.ui 1.0

import "../stores"
import "../helpers"
import "../stores" as Stores

View {
    id: root
    objectName: "StackView"
    property Stores.StackStore store

    Connections {
        target: store
        onRequestView: {
            print('request view', view, component)
            stack.replace(null, component, args);
        }
    }

    QQC2.StackView {
        id: stack
        anchors.fill: parent
    }
}
