import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import "../stores"

Page {
    id: root
    implicitWidth: 800
    implicitHeight: 600

    property RootStore store

    Button {
        anchors.centerIn: parent
        text: "Content"
    }
}