import click
from qautoadmin.utils.options import pass_options
from qautoadmin.utils.gen import Generator
from path import Path
import logging

_log = logging.getLogger(__name__)

here = Path(__file__).abspath().parent


@click.command('generate', short_help='EXPERIMENTAL: creates a new aspect')
@click.argument('aspect', type=click.Choice(['view', 'store']), required=True)
@click.argument('app', required=True)
@click.argument('name', required=True)
@pass_options
def app(opts, aspect, app, name):
    """EXPERIMENTAL: Generates a new aspect of your project in the named app.
    The aspect is generated inside the given application using name as a parameter

    \b
    Supported aspects:
    * `store`: interface to the services and container for logic
    * `view`: highlivel UI component using a store
    """
    _log.info('generate')
    click.secho('EXPERIMENTAL FEATURE. USE WITH CARE.', fg='red')
    path = opts.home
    opts.log('generate aspect %s in project %s', aspect, click.format_filename(path))
    g = Generator(path=here / 'templates')
    g.destination = path
    g.context.update({
        'app': app,
        'name': name,
    })
    if aspect == 'view':
        g.write('{{app}}/views/{{name}}.qml', 'generate/View.qml')
    elif aspect == 'store':
        g.write('{{app}}/stores/{{name}}.qml', 'generate/Store.qml')
