import click
from qautoadmin.utils.options import pass_options, relpathto
from qautoadmin.utils.gen import Generator
from path import Path
import logging

_log = logging.getLogger(__name__)

here = Path(__file__).abspath().parent


def generate_single_process(opts, path):
    dst = path
    project = Path(path).name
    print(project, dst)
    g = Generator(path=here / 'templates', context={'project': project, 'dst': dst})
    g.destination = dst
    g.source = 'new/simple'
    g.write('Main.qml')
    g.write('{{project}}.qmlproject', 'project.qmlproject')
    g.write('qtquickcontrols2.conf')
    g.write('stores/RootStore.qml')
    g.write('views/WelcomeView.qml')
    g.write('views/StackView.qml')
    g.write('views/CounterView.qml')
    g.write('views/ViewRegistry.qml')
    g.write('tests/tst_stackstore.qml')
    g.write('tests/tst_viewstack.qml')
    g.write('stores/RootStore.qml')
    g.write('stores/StackStore.qml')
    g.write('stores/CounterStore.qml')
    g.write('panels/StatusBar.qml')
    g.write('imports/sys/ui/qmldir')
    g.write('imports/sys/ui/Store.qml')
    g.write('imports/sys/ui/View.qml')
    g.write('helpers/qmldir')
    g.write('helpers/Notifier.qml')
    g.write('helpers/utils.js')
    g.write('helpers/viewstack.js')
    opts.set_script("start", "qmlscene -I {0}/imports {0}/Main.qml".format(relpathto(path)))

    click.echo('run the UI form the project folder with `qmlscene -I imports Main.qml`')


def generate_multi_process(opts, name):
    g = Generator(path=here / 'templates')
    g.destination = name
    g.source = 'new/appman'
    g.write('Main.qml')
    g.write('sysui/MainWindow.qml')
    g.write('sysui/views/WelcomeView.qml')
    g.write('sysui/stores/RootStore.qml')
    g.write('sysui/panels/StatusBar.qml')
    g.write('sysui/helpers/views.js')
    g.write('sysui/helpers/Notifier.qml')
    g.write('sysui/helpers/qmldir')


@click.command('new', short_help='EXPERIMENTAL: creates a new qauto project')
@click.option(
    '--template',
    type=click.Choice(['single', 'multi']),
    default='single',
    help='Project template. Default is "single" a single-process UI'
)
@click.argument('path', required=True, type=click.Path(resolve_path=True))
@pass_options
def app(opts, template, path):
    """EXPERIMENTAL: creates a new qauto project using a template.

    \b
    The following templates are supported:
    * `single` - single-process UI for small or simple user interfaces
    * `multi` - A multi process UI for larger or complex user interfaces
    """
    _log.info('create project in %s', click.format_filename(path))
    click.secho('EXPERIMENTAL FEATURE. USE WITH CARE.', fg='red')
    if template == 'single':
        generate_single_process(opts, path)
    elif template == 'multi':
        generate_multi_process(opts, path)
