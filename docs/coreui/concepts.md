# CoreUI Foundation

CoreUI is founded on some core-principles collected over the years, which all contribute to the overall design decisions. These principles are based on the opinions of many but may not be applicable to all.

## Agile Principles

CoreUI believes in the core values of the Manifesto for Agile Software development:

__Individuals and Interactions__ over processes and tools
:   Tools and processes are important, but it is more important to have competent people working together effectively.

__Working Software__ over comprehensive documentation
:   Good documentation is useful in helping people to understand how the software is built and how to use it, but the main point of development is to create software, not documentation.

__Customer Collaboration__ over contract negotiation
:   A contract is important but is no substitute for working closely with customers to discover what they need. 

__Responding to Change__ over following a plan
:   A project plan is important, but it must not be too rigid to accommodate changes in technology or the environment, stakeholders' priorities, and people's understanding of the problem and its solution.

_That is, while there is value in the items on the right, they value the items on the left more._ [^AgilePrinciples]

Sources: [Wikipedia](https://en.wikipedia.org/wiki/Agile_software_development), [Agile Manifesto](http://agilemanifesto.org)

## Live-Reloading Everything

Live reloading is the principle of reloading the program as a whole or as a piece on a defined change in the underlying source code. The change can be either triggered by any key-stroke, based on document persistence or time based. The reloading can be be either state preserving or state resetting.

Enabling hot reloading throughout the CoreUI architecture serves several goals: __Productivity__, __Testability__, __UX Creation__.

CoreUI not only supports live-reloading of the whole program but also encourages the developers to live-reload individual pieces of of the user interface. As such is forces the developer to think about components and their dependencies, thus resulting into a better testability. 

CoreUI also enforces a clean abstraction of business-logic and visual-logic, thus making the reloading of business tests part of the daily work-flow.

Reloading plays also a crucial part into enabling great UX by making the conversation the developer has with the UI more fluent.

## Testability and Dependency

CoreUI separated UI pieces into different components which a clear defined dependency. Also CoreUI forces the developer to much create more smaller components and work with re-use.

These managed dependencies and smaller components enables testability without much headaches.

CoreUI comes with a test harness which can test your component visually by launching either several components in different states or can run these component test automatically. All of this paired with live-reloading these tests when either the tests or the underlying components change.

## Separation of HW and SW

## UX Design Conversations

## Thinking in Components

## Components vs. Processes

## Aspects driven


