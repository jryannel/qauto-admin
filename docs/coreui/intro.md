# CoreUI Introduction

CoreUI is set of patterns and components to support a common user interface structure. It fosters the creation of a great user experience by focusing on the UI creation process. The CoreUI architecture has grown from the creation and re-creation of user interface projects for the different markets and especially the automotive markets. After several re-creation of user interface projects for vertical markets some pattern started to emerge. The CoreUI architecture as presented here is the destillation of these patterns.

CoreUI is an embedded development framework using the Qt5 and the QML/JS language. Programming embedded software user interface can be often unecessary complicated. CoreUI was designed to make the programming of these user interfaces easier. CoreUI makes assumptions about what every developers needs to get started to create stunning user interfaces. It allows you to develop user interfaces writing less code and to accomplish more.

CoreUI wants to bring back the fun and lightness to user interface development, so that we all can focus on the user experience again.

CoreUI is build of opinions of others. These opinions makes assumption what is the best way to create user interfaces. CoreUI is designed to use it that way and discourages other ways. If you master CoreUI you will probably experience a spike in productivity. If you keep with your old happits and try to bend CoreUI to your old ways of working you will have less happy experience.

## Fundamentals

CoreUI is based on several fundamental idioms, describing the core understanding of the UI creation process.

* Follow the Agile Principles  - *UX depends on flexibility, agility. Keep it that way.*
* Enable Hot-Reloading everywhere. - *Allow changes to be visible in a fraction of a second.*
* Testability and dependency management is paramount. - *Code what you can not test you can not trust. And trust if the foundation of large systems.*
* Separation of Hardware and Software - *As a UI developer you do not want on something you can not control or is not even available early on.*
* UX Design is a bi-directional conversation - *Design has a great impact on the UX, but also the choosen technology. Finding the balance and pushing forward, together makes heros.*
* Thinking in Components and Building Blocks - *Component based programming lead to a better structure and allows building larger user interfaces.*
* Component separation is good, process separation is better - *Trust is good, having control is better. A process gives you control.*
* Aspect driven components - *Design your component layers around different aspects of your software. Each aspect provides another piece to the puzzle*

A detailed discussion is available in the CoreUI Concepts chapter.
