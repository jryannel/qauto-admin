# Service Introduction

A service is a piece of vertical communication. One layer calls down on a different layer. In this case the UI layer calls down into the application services layer. These services are typical in a different process and a IPC transport is required. Also security mechanism need to be in place on the transport level.

!!!note
    Vertical communication communicates through the different layer of a software stack. Horizontal communication is between components on the same software layer.