# Welcome to QAuto Admin Guide

The `qauto-admin` script allows you to control your QtAuto installation. QtAuto is a set of components on top of Qt5 which allows you to create a typical user interface for the automotive market.

To better support the market needs the script propagates an opinionated architecture called the CoreUI Architecture. The script is capable of building the underlying platform as also to generate CoreUI projects and core components of these projects. 

You can read more about the CoreUI architecture in the CoreUI section of the guide.

The script supports your work-flow in several ways:

- Cloning, building the qauto repositories and Qt5 as also your custom projects
- Creating and running qauto based projects based on templates for single or multi-process architecture
- Adding core components based on the CoreUI architecture to a CoreUI project
- Running scripts to support your custom work-flow
- Managing your custom environment variable setup

You can read more about the individual features in the concepts section of the admin guide.

## Setting up QAuto stack

Create a project directory

    mkdir qauto && cd qauto

Write the config file `qauto.yml`

    qauto-admin init

Configure `qmake` to use existing Qt5 SDK

    qauto-admin config qmake <path/to/qmake>

Clone source repositories

    qauto-admin clone

Build all repositories

    qauto-admin build

Note: Please make sure that all QAuto components are successfully configured. 

Start the neptune3 ui

    qauto-admin start


## Setting up a new CoreUI project

    qauto-admin new myproject
    qauto-admin generate app counter
