# Quick Start

To use the admin we first need to install it as part of our environment.


    git clone git@git.pelagicore.net:uxteam/qauto-admin.git
    cd qauto-admin
    pip install -e .

The simplest start is to use an existing QtSDK and a single-process UI. This is a great start into the creation of a new user interface.

We first let the admin know where the existing Qt SDK is located. For this we point it to the `qmake` executable for the SDK.

    qauto-admin config qmake ~/Qt/5.10.0/clang_64/bin/qmake

Now we can create the single-process UI

    qauto-admin new single-ui --template single

We can launch the UI using the start script, which was registered by the project generator.

    qauto-admin start

To develop with the newly created UI you can open QtCreator and open the `single-ui/single-ui.qmlproject` project.


