# QtAutomotive Setup

!!!info
    This chapter requires that you successfully installed the `qauto-admin` script. Please see the installation chapter for guidance.

To create your first project you first need to have QtAuto installed. The easiest way is to use an existing Qt installation and install the remaining QtAuto modules. A more advanced option is to compile your own Qt5 and then install the QtAuto modules. A own Qt build is required if you are using linux and want to use the multi-process mode, as it requires a qt build with wayland support.

The `qauto-admin` tool will support you in both scenarios.

### Setup using existing Qt

Please install Qt5 using the Qt online installer first. Now you should create a folder to host the QtAuto components and initialize the folder.

    mkdir qauto
    cd qauto
    qauto-admin init

The init command will create a `qauto.yml` project document. You can either edit the document yourself or add config values.

To edit the configuration just type

    qauto-admin config --edit

This will start your default editor and open the qauto document.

Now we will tell qauto where the existing qt installation is by passing the path to the `qmake` executable.

For example on MACOS this should look like this:

    qauto-admin config qmake ~/Qt/5.10.0/clang_64/bin/qmake

From now on qauto-admin will use the existing Qt installation as base. 

!!!tip
    In case you need to build your own Qt leave the qmake configuration empty and qt command to build your custom Qt5 from source. See the section *Setup using custom Qt*.

Now we clone the QtAuto modules and build them.

First we can check the `auto` target using

    qauto-admin targets auto

This will print the currently listed repositories vailable under the auto target. A target is an ordered list or repositories. The ordering defines the build order. This information is stored in the `qauto.yml` targets section.

    ______name______|______________________repos______________________
    auto            | appman, dlt-daemon, qtivi, neptune3-ui

The list may vary based on your `qauto.yml` configuration. The next step would be to clone and build all auto repositories.

    qauto-admin clone auto
    qauto-admin build auto

The clones repositories are available in the `source/<repo-name>` and `build/<repo-name>` locations. After building the repositories will be automatically installed into `install/<repo-name>`. Be aware some repositories will automatically install as qt modules into the Qt directory and can not be found in the install location.

If you later want to update your installation, you can simple run update.

    qauto-admin update auto
    qauto-admin build auto

To clean the build you can run

    qauto-admin clean auto

### Setup using custom Qt

If you want to use the multi-process setup on Linux or just want to use a custom Qt the script will support you in building Qt for your platform.

The first step is to check if your OS can build Qt and the QAuto components. For this run the OS command.

    qauto-admin os --check

Note: The OS command is currently only supported on Ubuntu currently.

The check command will either be positive or negative. In the case the result is negative please run the os init command.

    qauto-admin os --init

This command will either print the required steps to initialize your OS or ask you to install several packages onto your system. For this step administration privileges are required.

After the OS configuration has been validated Qt can now be downloaded and build.

    qauto-admin qt --clone
    qauto-admin qt --config
    qauto-admin qt --build

The last step can take up to an hour, depending on your machine configuration.

To set the number of used make jobs (how many CPU cores make can use), you can edit the jobs config value. For example for a great performance on a Core i7 Quad Core Intel CPU you can set the jobs count to 6. You would still have two processors left to continue working.

    qauto-admin config jobs 6

To check your configuration you can list the configurtion values.

    qauto-admin config

Will output something like this

    build            | build
    install          | install
    jobs             | 6
    qmake            | ~/Qt/5.10.0/clang_64/bin/qmake
    source           | source

To unset a configuration value you can use the `config --unset` option.

    qauto-admin config --unset qmake


