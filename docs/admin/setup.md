# `QAuto-Admin` Script Setup

`qauto-admin` is a script which can be easily installed.

## Requirements

The script requires a python (2.x or 3.x ) installation with the PIP package manager installed.

## Repository Based Setup

The repository based setup allow you to clone the original repository and track all changes by just updating the cloned repository.

    git clone git@git.pelagicore.net:uxteam/qauto-admin.git
    cd qauto-admin
    pip install -e .

To update the installation you need to update the repository.

    cd qauto-admin
    git pull

## Uninstall

To uninstall the script you need to use the pip tool

    pip uninstall qauto-admin

## First Usage

After the installation the `qauto-admin` command is at your disposal.

    qauto-admin --help




