# QAuto Admin Introduction

The `qauto-admin` script allows you to control your QtAuto installation. QtAuto is a set of components on top of Qt which allows you to create a typical user interface for the automotive market.

The script supports your QAuto work-flow in several ways:

- Cloning, building the qtauto repositories and Qt as also your custom projects
- Creating and running qtauto based projects (single or multi-process)
- Running arbitrary scripts to support your custom work-flow
- Managing your custom environment variable setup

You can read more about the individual features in the concepts section of the guide.

## Commands

* `qauto-admin init` - Initializes current folder
* `qauto-admin qt` - Clones and build Qt5
* `qauto-admin clone` - Clones QAuto components
* `qauto-admin build` - Build QtAuto components.
* `qauto-admin os` - Initializes an Ubuntu system
* `qauto-admin run` - Start the current active system ui
* `qauto-admin dev` - Creates a development environment for contribution
* `qauto-admin help` - Print this help message.

`qauto-admin <command> --help` will print detailed information about the command.
