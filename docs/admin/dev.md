# Developing and Contributing

To develop on a listed repository you need to clone the repositories in an independent folder and build it locally. This separation helps to have always a reference build available based on the upstream source code and a developer build based on your current modified stand.

`qauto-admin` helps you here using the `dev` command. The `dev` command allows you to clone and configure a repository inside a `dev` folder. If you project has support for Qt code-review `qauto-admin` will install the code-review commit templates and add gerrit as a remote repository. To mark a repository for code-review add a code-review property in the repository section of your config document with the name of the review path.

    neptune3-ui:
        url: git://code.qt.io/qt-apps/neptune3-ui.git
        branch: '5.11'
        build: qmake
        os: [linux, macos]
        codereview: "qt-apps/neptune3-ui"


You use the `dev` command like this:

    qauto-admin dev neptune3-ui

This will create a `dev/source/neptune3-ui` and `dev/build/neptune3-ui` and after a successful build a `dev/install/neptune3-ui`. These folders are independent from your upstream tracking folders.

If you like to see what the command are doing, you can simple use the `--dry-run` option.

The command will output a small message how to configure Qt Creator to build and run your project.



