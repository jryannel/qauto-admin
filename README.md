

*THIS SCRIPT IS OUTDATED. IT WILL BE SUPERSEDED SOON BY A NEW SCRIPT*



# QAuto Admin Tool

The QAutoAdmin tool `qauto-admin` is a helper tool to create a QtAuto project
from scratch.

In general the script is build around two concepts:
- repos and build management
- code generation

The repo and build management allows you to add qmake and cmake based repositories to a settings file and lists them as targets. The targets can then be build using the script.

The code generation part allows you to create standardized code layouts for single and multi-process user interfaces using the new CoreUI architecture. The generators also allow to run the projects and add individual UI components to the code structure.

A more in-depth documentation can be found at: https://jryannel.gitlab.io/qauto-admin/

## Installation from Git

You can also direcly install from git using the python package manager

    pip install git+https://gitlab.com/jryannel/qauto-admin.git@develop --upgrade

Note: On Ubuntu you might need to add `$HOME/.local/bin` to you `$PATH`

## Editable Installation

    git clone https://gitlab.com/jryannel/qauto-admin.git
    cd qauto-admin
    pip install -e .

To update the installation you need to update the repository.

    cd qauto-admin
    git pull

To uninstall the script you need to use the pip tool

    pip uninstall qauto-admin

## Using Virtualenv

In case you do not want to pollute your local python installation you can use python virtualenv

    virtualenv -p python3 venv
    source venv/bin/activate

Now install qauto-admin and to exit this python virtual environment call `deactivate`.


## Building QtAuto

First we need to seup our qauto configuration and populate it with our repositories:

    mkdir qauto
    cd qauto
    qauto-admin init

If you like to modify the configuration you can simply call

    qauto-admin config --edit

A typical value would be to set the qmake config to your existing Qt installation.

    config:
        qmake: ~/Qt/5.11.0/clang_64/bin/qmake

The same could be achieved using this call:

    qauto-admin config qmake ~/Qt/5.11.0/clang_64/bin/qmake

To see the current configuration just call

    qauto-admin config

Next you need to clone the repositories first to get hold onto the upstream source code.

    qauto-admin clone auto

To see which targets and repositories are available you can call

    qauto-admin targets
    qauto-admin repos

To build the stack call

    qauto-admin build auto

## Create a new UI

`qauto-admin` comes with two sample UI structures, single and multi-process. The admin tool allows you to scaffold a simple project.

    qauto-admin new uisingle --template single
    cd uisingle
    qmlscene -I imports Main.qml

For a multiprocess ui you can call

    qauto-admin new uimulti --template multi
    cd uimulti
    appman -c am-config.yml

## Additional Features

There are more features available using qauto-admin for example

* Create distinct components for the user interface
* Manage user environment variable using an env document
* Build different targets than qtauto
* Add custom scripts to qauto-admin

See https://jryannel.gitlab.io/qauto-admin/ for more information.

/ jryannel
