from qautoadmin.utils.options import Options
from path import Path

here = Path(__file__).parent
data_dir = here / 'data'
workspace = data_dir / 'workspace'
sub_folder = workspace / 'subfolder'


def test_config_file_loading():
    workspace.chdir()
    opts = Options()
    assert opts.workspace_path == workspace


def test_config_file_resolution():
    sub_folder.chdir()
    opts = Options()
    assert opts.workspace_path == workspace

def test_loading_env():
    workspace.chdir()
    opts = Options()
    env = opts.env
    assert env.get('KEY') == 'VALUE'
    assert env.get('USER_HOME') == Path('~').expanduser()

def test_script():
    workspace.chdir()
    opts = Options()
    script = opts.script('install')
    assert script == 'make install'

def test_repos():
    workspace.chdir()
    opts = Options()
    repo = opts.repo('qmllive')
    assert repo["branch"] == "5.8"
    